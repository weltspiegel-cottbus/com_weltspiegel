<?php

// No direct access to this file
use Joomla\CMS\MVC\Controller\BaseController;

defined('_JEXEC') or die('Restricted access');

/**
 * Weltspiegel Component Controller
 *
 * @since  0.0.2
 */
class WeltspiegelController extends BaseController {
}
