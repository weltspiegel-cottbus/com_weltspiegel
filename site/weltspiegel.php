<?php

use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Controller\BaseController;

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

try
{
  // Get an instance of the controller prefixed by HelloWorld
  $controller = BaseController::getInstance('Weltspiegel');

  // Perform the Request task
  $input = Factory::getApplication()->input;
  $controller->execute($input->getCmd('task'));

  // Redirect if set by the controller
  $controller->redirect();
}
catch (Exception $e)
{
  /** @noinspection PhpUnhandledExceptionInspection */
  throw new Exception('Ups, das sollte nicht passieren.', 500);
}



