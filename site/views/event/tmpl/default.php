<?php

use LeanStack\CinetixxAPI\Model\Event;

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/** @var Event $event */
$event = $this->event;
?>
<h2><?= $event->getTitle() ?></h2>
