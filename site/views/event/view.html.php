<?php

use Joomla\CMS\MVC\View\HtmlView;
use LeanStack\CinetixxAPI\Model\Event;

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Events HTML View class for the Weltspiegel Component
 * @since  0.0.2
 */
class WeltspiegelViewEvent extends HtmlView
{
  /**
   * @var Event
   * @since 0.0.5
   */
  public $event;


  /**
   * Display the Event view
   * @since  0.0.2
   *
   * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
   *
   * @return void
   * @throws Exception
   *
   */
	function display($tpl = null)
	{
    // Assign data to the view
    $this->event = $this->get('Item');

    // Check for errors.
    if (count($errors = $this->get('Errors')))
    {
      throw new Exception(implode("\n", $errors), 500);
    }

    // Display the view
    parent::display($tpl);
	}
}
