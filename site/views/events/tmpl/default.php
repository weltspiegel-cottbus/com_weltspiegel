<?php

use LeanStack\CinetixxAPI\Model\Event;

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/** @var Event[] $events */
$events = $this->events;

?>
<section class="events">
  <?php foreach ($events as $event): ?>
  <article>
    <h2><?= $event->getTitle() ?></h2>
  </article>
  <?php endforeach; ?>
</section>
