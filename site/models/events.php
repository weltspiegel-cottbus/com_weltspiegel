<?php

use Joomla\CMS\Application\SiteApplication;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\ItemModel;
use LeanStack\CinetixxAPI\CinetixxClient;
use LeanStack\CinetixxAPI\Model\Event;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\HttpClient;

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Events Model
 * @since  0.0.4
 */
class WeltspiegelModelEvents extends ItemModel
{

  /**
   * @var CinetixxClient
   * @since version 0.0.5
   */
  protected $cinetixxClient;

  /**
   * WeltspiegelModelEvents constructor.
   *
   * @param   array  $config
   *
   * @throws Exception
   * @since  0.0.5
   *
   */
  public function __construct(array $config = [])
  {
    parent::__construct($config);

    /** @var SiteApplication $app */
    $app = Factory::getApplication();

    $mandatorId = intval($app->getParams()->get('mandatorid'));
    $client = HttpClient::create();
    $cache = new FilesystemAdapter('cinetixx', 3600, JPATH_BASE . '/.cache');
    $this->cinetixxClient = new CinetixxClient($mandatorId, $client, $cache);
  }

  /**
   * @param  null  $pk
   *
   * @return Event[]
   *
   * @throws Exception
   * @throws \Psr\Cache\InvalidArgumentException
   * @since  0.0.4
   */
  public function getItem($pk = null)
  {
    return $this->cinetixxClient->getEvents();
  }
}
