<?php

use Joomla\CMS\Application\SiteApplication;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\ItemModel;
use LeanStack\CinetixxAPI\CinetixxClient;
use LeanStack\CinetixxAPI\Model\Event;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\HttpClient;

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Event Model
 * @since  0.0.4
 */
class WeltspiegelModelEvent extends ItemModel
{

  /**
   * @var SiteApplication
   * @since 0.0.5
   */
  protected $application;

  /**
   * @var CinetixxClient
   * @since 0.0.5
   */
  protected $cinetixxClient;

  /**
   * WeltspiegelModelEvents constructor.
   *
   * @param   array  $config
   *
   * @throws Exception
   * @since  0.0.5
   *
   */
  public function __construct(array $config = [])
  {
    parent::__construct($config);

    $this->application = Factory::getApplication();

    $mandatorId = intval($this->application->getParams()->get('mandatorid'));
    $client = HttpClient::create();
    $cache = new FilesystemAdapter('cinetixx', 3600, JPATH_BASE . '/.cache');
    $this->cinetixxClient = new CinetixxClient($mandatorId, $client, $cache);
  }

  /**
   * @param   null  $pk
   *
   * @return Event
   * @throws \Psr\Cache\InvalidArgumentException
   * @since  0.0.4
   */
  public function getItem($pk = null)
  {
    $eventId = intval($this->application->input->get('eventid'));
    return $this->cinetixxClient->getEvent($eventId);
  }
}

