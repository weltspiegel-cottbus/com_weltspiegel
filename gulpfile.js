const gulp = require('gulp');
const zip = require('gulp-zip');

exports.default = () => (
  gulp.src(['site/**/*', 'admin/**/*', 'weltspiegel.xml'], {base: '.'})
    .pipe(zip('com-weltspiegel.zip'))
    .pipe(gulp.dest('.'))
);
